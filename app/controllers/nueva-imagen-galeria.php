<?php
require_once __DIR__. '/../../utils/utils.php';
require_once 'exceptions/FileException.php';
require_once 'exceptions/AppException.php';
require_once 'exceptions/QueryException.php';
require_once 'utils/File.php';
require_once 'entity/ImagenGaleria.php';
require_once 'entity/Categoria.php';
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'core/App.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';

try
{

        $descripcion = trim(htmlspecialchars($_POST['descripcion']));
        $categoria = trim(htmlspecialchars($_POST['categoria']));

        if(empty($categoria))
            throw new ValidationException('No se ha recibido la categoria');

        $tiposAceptados = ['image/jpeg','image/png', 'image.gif'];
        $imagen = new File('imagen', $tiposAceptados);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

        $imgRepository = new ImagenGaleriaRepository();
        $imgRepository->guarda($imagenGaleria);


}

catch (FileException $fileException)
{
    die($fileException->getMessage());
}
catch (QueryException $queryException)
{
    die($queryException->getMessage());
}
catch (ValidationException $validationException)
{
    die($validationException->getMessage());
}

App::get('router')->redirect('9/repositorio_alali/imagenes-galeria');