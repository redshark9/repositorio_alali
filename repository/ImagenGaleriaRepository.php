<?php
require_once __DIR__ . '/../database/QueryBuilder.php';

class ImagenGaleriaRepository extends QueryBuilder
{
    /**
     * ImagenGaleriaRepository constructor.
     * @param string $table
     * @param string $classEntity
     */
    public function __construct(string $table='imagenes', string $classEntity='ImagenGaleria')
    {
        parent::__construct($table, $classEntity);
    }

    /**
     * @param ImagenGaleria $imagenGaleria
     * @return Categoria
     * @throws QueryException
     */
    public function getCategoria(ImagenGaleria $imagenGaleria): Categoria
    {
        $categoriaRepository = new CategoriaRepository();

        return $categoriaRepository->find($imagenGaleria->getCategoria());
    }

    /**
     * @param ImagenGaleria $imagenGaleria
     * @throws QueryException
     */
    public function guarda(ImagenGaleria $imagenGaleria)
    {
        $fnGuardaImagen = function () use ($imagenGaleria)
        {
            $categoria = $this->getCategoria($imagenGaleria);
            $categoriaRepository = new CategoriaRepository();
            $categoriaRepository->nuevaImagen($categoria);

            $this->save($imagenGaleria);
        };

        $this->executeTransaction($fnGuardaImagen);
    }
}