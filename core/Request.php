<?php

class Request
{

    public static function uri()
    {
        $url = preg_split("/\//", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        return end($url);
    }

    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}