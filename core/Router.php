<?php
class Router
{
    private $routes;

    /**
     * Router constructor.
     */
    private function __construct()
    {
        $this->routes = [
            'GET' => [],
            'POST' => []
        ];
    }

    /**
     * @param $file
     * @return Router
     */
    public static function load(string $file)
    {
        $router = new Router();
        require $file;
        return $router;
    }

    /**
     * @param $uri
     * @param $controller
     */
    public function get(string $uri, string $controller): void
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * @param $uri
     * @param $controller
     */
    public function post(string $uri, string $controller): void
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * @param string $uri
     * @param $method
     * @return string
     * @throws NotFoundException
     */
    public function direct(string $uri, string $method): string
    {
        if (array_key_exists($uri, $this->routes[$method]))
        {
            return $this->routes[$method][$uri];
        }
        throw new \NotFoundException(
            'No se ha definido una ruta para esta URI');
    }

    public function redirect(string $path)
    {
        header('location: /' . $path);
    }


}